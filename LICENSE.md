**Copyright (c) 2018 Intelligence Fusion Limited <info@intelligencefusion.co.uk>  
All Rights Reserved**

**NOTICE:** All information contained herein is, and remains the property of Intelligence Fusion Limited and
their suppliers, if any. The intellectual and technical concepts contained herein are
proprietary to Intelligence Fusion Limited and their suppliers and may be covered by U.K. and Foreign
Patents, patents in process, and are protected by trade secret or copyright law.
Dissemination of this information or reproduction of this material is strictly forbidden
unless prior written permission is obtained from Intelligence Fusion Limited.