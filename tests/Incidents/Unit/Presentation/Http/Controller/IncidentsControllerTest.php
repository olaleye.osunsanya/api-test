<?php

namespace Tests\Incidents\Unit\Presentation\Http\Controller;

use Fusion\Incidents\Application\Command\CreateIncidentCommand;
use Fusion\Incidents\Domain\ValueObject\IncidentId;
use Fusion\Incidents\Presentation\Http\Controller\IncidentsController;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\QueryBus;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;

class IncidentsControllerTest extends TestCase
{
    /**
     * @var CommandBus|ObjectProphecy
     */
    private $commandBus;
    /**
     * @var QueryBus|ObjectProphecy
     */
    private $queryBus;
    /**
     * @var IncidentsController
     */
    private $controller;

    public function setUp()
    {
        $this->commandBus = $this->prophesize(CommandBus::class);
        $this->queryBus = $this->prophesize(QueryBus::class);
        $this->controller = new IncidentsController(
            $this->commandBus->reveal(),
            $this->queryBus->reveal()
        );
    }

    public function test_store_returns_201_response_containing_id_of_created_incident_resource()
    {
        $body = (object) [
            'description' => 'Man caught with explosives',
            'reportedAt' => 1596051163,
            'categoryId' => 'be350de0-1004-4fce-ad17-52ec577d2b49',
            'categoryName' => 'Terrorism',
            'categoryColour' => '#ffffff',
            'position' => (object) [
                'longitude' => -51.591839,
                'latitude' => 12.409182,
            ]
        ];

        $request = Request::create('POST', '/incidents', [], [], [], [], json_encode($body));

        $commandAssertion = Argument::that(function (CreateIncidentCommand $command) {
            return $command->getIncidentId() instanceof IncidentId;
        });

        $this->commandBus->dispatch($commandAssertion)->shouldBeCalled();

        $response = $this->controller->store($request);
        $responseBody = json_decode($response->getContent());

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertTrue(isset($responseBody->incidentId));
    }

    public function test_should_create_incident()
    {
        $parameter = [
            'description' => 'Man caught with explosives',
            'reportedAt' => 1596051163,
            'categoryId' => 'be350de0-1004-4fce-ad17-52ec577d2b49',
            'categoryName' => 'Terrorism',
            'categoryColour' => '#ffffff',
            'position' => (object) [
                'longitude' => -51.591839,
                'latitude' => 12.409182,
            ],
            'involvedParty' => (object)[
                'id' => 'ac6c736d-d1dd-4ec7-9661-43a3322cde3c',
                'name' => 'President of United States',
                'involvement' => 'Directly Targeted'

            ]

        ];

        $request = Request::create('POST', '/incidents', [], [], [], [], json_encode($parameter));
        $this->assertTrue(isset(json_decode($request->getContent())->involvedParty));
        $response = $this->controller->store($request);
        $responseBody = json_decode($response->getContent());

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertTrue(isset($responseBody->incidentId));
    }

    public function test_should_return_incident()
    {
        $request = Request::create('GET', '697b4c63-6360-415e-93c7-b598e7a49cba', [], [], [], [], '');
        $response = $this->controller->show($request);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
