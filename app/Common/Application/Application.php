<?php

declare(strict_types=1);

namespace Fusion\Common\Application;

use Laravel\Lumen\Application as LumenApplication;

final class Application extends LumenApplication
{
    /**
     * @inheritDoc
     */
    public function isDownForMaintenance()
    {
        return filter_var(env('APP_MAINTENANCE', 'false'), FILTER_VALIDATE_BOOLEAN);
    }
}
