<?php

declare(strict_types=1);

namespace Fusion\Common\Application\Event;

use Prooph\Common\Messaging\Command;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\CommandDispatchException;

/**
 * Event Handler
 *
 * @package Fusion\Common\Application\Event
 * @author  James Drew <james.drew@growthcapitalventures.co.uk>
 * @version 1.0.0
 */
abstract class EventHandler
{
    /** @var CommandBus */
    protected $commandBus;

    // Setup ----

    /**
     * CommandHandler constructor.
     *
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    // Internals ----

    /**
     * Dispatch a command and handle the outcome
     *
     * @param Command $command
     *
     * @throws \Exception
     */
    protected function dispatchCommand(Command $command): void
    {
        try {
            $this->commandBus->dispatch($command);
        } catch (CommandDispatchException $exception) {
            throw $exception->getPrevious();
        }
    }
}
