<?php

declare(strict_types=1);

namespace Fusion\Common\Application\Event;

use Illuminate\Queue\SerializesModels;

abstract class Event
{
    use SerializesModels;
}
