<?php

namespace Fusion\Common\Application\User;

class Subscription
{
    /**
     * @var Restrictions|null
     */
    private $restrictions;

    public function setRestrictions(Restrictions $restrictions): self
    {
        $this->restrictions = $restrictions;
        return $this;
    }

    public function getRestrictions(): ?Restrictions
    {
        return $this->restrictions;
    }
}
