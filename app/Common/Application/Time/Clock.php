<?php

namespace Fusion\Common\Application\Time;

interface Clock
{
    public function now(): \DateTimeImmutable;
}
