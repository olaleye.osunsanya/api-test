<?php

declare(strict_types=1);

namespace Fusion\Common\Application\Command;

use Prooph\Common\Messaging\Command;
use Prooph\EventStore\Exception\ConcurrencyException;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\CommandDispatchException;

/**
 * Dispatches Commands Trait
 *
 * @package Fusion\Common\Application\Command
 * @author  James Drew <james.drew@growthcapitalventures.co.uk>
 */
trait DispatchesCommands
{
    /** @var CommandBus */
    protected $commandBus;

    /**
     * Dispatch a command and handle the outcome
     *
     * @param Command $command
     *
     * @throws \Exception
     */
    protected function dispatchCommand(Command $command): void
    {
        $attemptsRemaining = 3;

        while ($attemptsRemaining > 0) {
            try {
                // Attempt the command
                $this->commandBus()->dispatch($command);

                return;
            } catch (CommandDispatchException $dispatchException) {
                $previousException = $dispatchException->getPrevious();

                if ($previousException instanceof ConcurrencyException) {
                    $attemptsRemaining--;

                    // If this was the last attempt, rethrow the concurrency exception
                    if ($attemptsRemaining < 1) {
                        throw $previousException;
                    }
                } else {
                    throw $previousException;
                }
            }
        }
    }

    /**
     * @return CommandBus
     */
    protected function commandBus(): CommandBus
    {
        if (!$this->commandBus instanceof CommandBus) {
            $this->commandBus = app(CommandBus::class);
        }

        return $this->commandBus;
    }
}
