<?php

declare(strict_types=1);

namespace Fusion\Common\Domain\Model;

/**
 * Invariant Exception
 *
 * This is to be used for cases where domain invariants fail, either as a base class or as an instance
 *
 * @package Fusion\Common\Domain\Model
 * @author  James Drew <james.drew@growthcapitalventures.co.uk>
 */
class InvariantException extends \RangeException
{
    /** @var string|null */
    private $propertyPath;
    /** @var mixed  */
    private $value;
    /** @var array */
    private $constraints;

    // Setup ----

    /**
     * InvariantException constructor.
     *
     * @param string      $message
     * @param int         $code
     * @param string|null $propertyPath
     * @param mixed       $value
     * @param array       $constraints
     */
    public function __construct(string $message, int $code = 0, ?string $propertyPath = null, $value = null, ?array $constraints = [])
    {
        parent::__construct($message, $code);

        $this->propertyPath = $propertyPath;
        $this->value        = $value;
        $this->constraints  = $constraints;
    }

    /**
     * User controlled way to define a sub-property causing
     * the failure of a currently asserted objects.
     *
     * Useful to transport information about the nature of the error
     * back to higher layers.
     *
     * @return string
     */
    public function getPropertyPath()
    {
        return $this->propertyPath;
    }

    /**
     * Get the value that caused the assertion to fail.
     *
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Get the constraints that applied to the failed assertion.
     *
     * @return array
     */
    public function getConstraints()
    {
        return $this->constraints;
    }
}
