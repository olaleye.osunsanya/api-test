<?php

namespace Fusion\Incidents\Application\Command;

use Fusion\Incidents\Domain\Factory\IncidentFactory;
use Fusion\Incidents\Domain\Repository\IncidentRepository;
use Prooph\ServiceBus\EventBus;

class CreateIncidentHandler
{
    /**
     * @var EventBus
     */
    private $eventBus;

    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    public function __invoke(CreateIncidentCommand $command)
    {
        $incident = IncidentFactory::create($command->getIncidentId(), $command->getData());
        $incidentRepository = new IncidentRepository();
        $incidentRepository->save($incident);
    }
}
