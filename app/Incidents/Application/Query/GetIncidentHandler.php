<?php

namespace Fusion\Incidents\Application\Query;

use Fusion\Incidents\Domain\Entity\Incident;
use Fusion\Incidents\Domain\Repository\IncidentRepository;
use React\Promise\Deferred;

class GetIncidentHandler
{
    public function __invoke(GetIncidentQuery $query, Deferred $promise): void
    {
        $promise->resolve((new IncidentRepository)->get($query->getIncidentId()));
    }
}
