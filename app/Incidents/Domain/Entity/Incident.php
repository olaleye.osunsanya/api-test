<?php

namespace Fusion\Incidents\Domain\Entity;

use DateTimeImmutable;
use Fusion\Incidents\Domain\ValueObject\IncidentId;
use Fusion\Incidents\Domain\ValueObject\IncidentCategory;
use Fusion\Incidents\Domain\ValueObject\InvolvedParty;
use Fusion\Incidents\Domain\ValueObject\Map\Geometry\Position;

class Incident
{
    /**
     * @var IncidentId
     */
    private $id;
    /**
     * @var string
     */
    private $description;
    /**
     * @var DateTimeImmutable
     */
    private $reportedAt;
    /**
     * @var IncidentCategory
     */
    private $category;
    /**
     * @var Position
     */
    private $position;

    /** @var InvolvedParty */
    private $involvedParty;

    /**
     * @param IncidentId $id
     * @param string $description
     * @param DateTimeImmutable $reportedAt
     * @param IncidentCategory $category
     * @param Position $position
     * @param InvolvedParty $involvedParty
     */
    public function __construct(IncidentId $id, string $description, DateTimeImmutable $reportedAt, IncidentCategory $category, Position $position, InvolvedParty $involvedParty)
    {
        $this->id = $id;
        $this->description = $description;
        $this->reportedAt = $reportedAt;
        $this->category = $category;
        $this->position = $position;
        $this->involvedParty = $involvedParty;
    }

    /**
     * @return IncidentId
     */
    public function getId(): IncidentId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getReportedAt(): DateTimeImmutable
    {
        return $this->reportedAt;
    }

    /**
     * @return IncidentCategory
     */
    public function getCategory(): IncidentCategory
    {
        return $this->category;
    }

    /**
     * @return Position
     */
    public function getPosition(): Position
    {
        return $this->position;
    }

    /**
     * @return InvolvedParty
     */
    public function getInvolvedParty(): InvolvedParty
    {
        return $this->involvedParty;
    }
}
