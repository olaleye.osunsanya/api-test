<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\Repository;

use Fusion\Incidents\Domain\Entity\Incident;
use Fusion\Incidents\Domain\ValueObject\IncidentId;
use Fusion\Common\Application\Exception\NotFoundException;
use Fusion\Incidents\Domain\ValueObject\IncidentCategory;
use Fusion\Incidents\Domain\ValueObject\InvolvedParty;
use Fusion\Incidents\Domain\ValueObject\Map\Geometry\Position;
use Illuminate\Support\Facades\DB;

final class IncidentRepository
{

    /**
     * @param Incident $incident
     */
    public function save(Incident $incident): void
    {
        DB::table('incidents')->insert([
            'id' => $incident->getId()->toString(),
            'description' => $incident->getDescription(),
            'reported_at' => $incident->getReportedAt()->getTimestamp(),
            'category_id' => $incident->getCategory()->getId(),
            'category_name' => $incident->getCategory()->getName(),
            'category_colour' => $incident->getCategory()->getColour(),
            'position' => $incident->getPosition()->toString(),
            'involved_party_id' => $incident->getInvolvedParty()->getId(),
            'involved_party_name' => $incident->getInvolvedParty()->getName(),
            'involved_party_involvement' => $incident->getInvolvedParty()->getInvolvement()
        ]);
    }

    /**
     * @param IncidentId $id
     * @return Incident
     * @throws NotFoundException
     */
    public static function get(IncidentId $id): Incident
    {
        $incident = DB::table('incidents')->find($id->toString());

        if ($incident === null) {
            throw new NotFoundException('Incident does not exist');
        }
        return new Incident(
            IncidentId::fromString($incident->id),
            $incident->description,
            new \DateTimeImmutable(date('Y-m-d', $incident->reported_at)),
            (new IncidentCategory($incident->category_id, $incident->category_name, $incident->category_colour)),
            Position::fromString($incident->position),
            (new InvolvedParty($incident->involved_party_id, $incident->involved_party_name, $incident->involved_party_involvement))
        );
    }
}
