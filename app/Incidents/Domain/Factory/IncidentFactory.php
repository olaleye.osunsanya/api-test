<?php

//declare(strict_types=1);

namespace Fusion\Incidents\Domain\Factory;

use Fusion\Incidents\Domain\Entity\Incident;
use Fusion\Incidents\Domain\ValueObject\IncidentId;
use Fusion\Incidents\Domain\ValueObject\IncidentCategory;
use Fusion\Incidents\Domain\ValueObject\InvolvedParty;
use Fusion\Incidents\Domain\ValueObject\Map\Geometry\Position;

class IncidentFactory
{

    /**
     * @param Incident $id
     * @param $data
     * @return Incident
     */
    public static function create(IncidentId $id, $data): Incident
    {
        return new Incident(
            $id,
            $data->description,
            \DateTimeImmutable::createFromFormat('U', $data->reportedAt),
            (new IncidentCategory($data->categoryId, $data->categoryName, $data->categoryColour)),
            Position::fromCoordinates($data->position->latitude, $data->position->longitude),
            (new InvolvedParty($data->involvedParty->id, $data->involvedParty->name, $data->involvedParty->involvement))
        );
    }
}
