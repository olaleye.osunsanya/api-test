<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\ValueObject\Map\Geography;

use Fusion\Common\Domain\Model\Assert;
use Fusion\Common\Domain\Model\ValueObject;

/**
 * Country
 *
 * @package Fusion\Incidents\Domain\Model\Map\Geography
 * @author  James Drew <james.drew@growthcapitalventures.co.uk>
 */
final class Country extends ValueObject
{
    /** @var string */
    protected $code;
    /** @var string|null */
    protected $isoAlpha2;
    /** @var string */
    protected $name;
    /** @var Continent */
    protected $continent;

    // Setup ----

    /**
     * Get a new country instance
     *
     * @param string    $code
     * @param string    $isoAlpha2
     * @param string    $name
     * @param Continent $continent
     *
     * @return Country
     */
    public static function describe(string $code, string $isoAlpha2, string $name, Continent $continent): self
    {
        return new self($code, $isoAlpha2, $name, $continent);
    }

    /**
     * Get a new country instance for a country without an ISO Alpha 2 code
     *
     * @param string    $code
     * @param string    $name
     * @param Continent $continent
     *
     * @return Country
     */
    public static function describeWithoutIsoCode(string $code, string $name, Continent $continent): self
    {
        return new self($code, null, $name, $continent);
    }

    /**
     * Parse a string representation of a country
     *
     * @param string $countryString
     *
     * @return Country
     */
    public static function fromString(string $countryString): Country
    {
        Assert::that($countryString)
            ->notEmpty()
            ->regex("/[A-Z]{3}:[\p{L}:\-\.\'\(\)\ ]*\([A-Z]{2}:[A-Za-z\ \(\)]+\)/u");

        $parts           = explode(':(', $countryString);
        $countryParts    = explode(':', $parts[0]);
        $continentString = substr($parts[1], 0, -1);

        $code  = $countryParts[0];

        if (count($countryParts) > 2) {
            $isoAlpha2 = $countryParts[1];
            $name      = $countryParts[2];

            return new self($code, $isoAlpha2, $name, Continent::fromString($continentString));
        }

        $name = $countryParts[1];

        return new self($code, null, $name, Continent::fromString($continentString));
    }

    /**
     * Country constructor.
     *
     * @param string      $code
     * @param null|string $isoAlpha2
     * @param string      $name
     * @param Continent   $continent
     */
    private function __construct(string $code, ?string $isoAlpha2, string $name, Continent $continent)
    {
        Assert::that($code)->notEmpty();

        $this->code      = $code;
        $this->isoAlpha2 = $isoAlpha2;
        $this->name      = $name;
        $this->continent = $continent;
    }

    // Queries ----

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @return null|string
     */
    public function isoAlpha2(): ?string
    {
        return $this->isoAlpha2;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return Continent
     */
    public function continent(): Continent
    {
        return $this->continent;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        $parts = [
            $this->code,
            $this->isoAlpha2,
            $this->name,
            "({$this->continent->toString()})"
        ];

        return implode(':', array_filter($parts));
    }
}
