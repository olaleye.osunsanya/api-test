<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\ValueObject\Map\Geometry;

interface Geometry
{
    // Commands ----

    /**
     * Get a new instance of this geometry with adjusted coordinates
     *
     * @param array $coordinates
     *
     * @return Geometry
     */
    public function withAdjustedCoordinates(array $coordinates): Geometry;

    // Queries ----

    /**
     * Get a string representation of this geometry
     *
     * @return string
     */
    public function toString(): string;

    /**
     * Get the simple type name for this geometry, e.g. Polygon
     *
     * @return string
     */
    public function type(): string;

    /**
     * Get the coordinates that make this geometry in a structured array
     *
     * @return array
     */
    public function coordinates(): array;
}
