<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\ValueObject\Map\Geometry;

use Fusion\Common\Domain\Model\Assert;
use Fusion\Common\Domain\Model\ValueObject;

final class Radius extends ValueObject
{
    /** @var float */
    private $length;

    // Setup ----

    /**
     * Parse a float representation of a radius
     *
     * @param float $length
     *
     * @return Radius
     */
    public static function fromFloat(float $length): Radius
    {
        return new self($length);
    }

    /**
     * Parse a string representation of a radius
     *
     * @param string $length
     *
     * @return Radius
     */
    public static function fromString(string $length): Radius
    {
        return new self((float) $length);
    }

    /**
     * Radius constructor.
     *
     * @param float $length
     */
    private function __construct(float $length)
    {
        Assert::that($length)->greaterThan(0, "Radii must be positive");

        $this->length = $length;
    }

    // Queries ----

    /**
     * Get the length of this radius as a float
     *
     * @return float
     */
    public function length(): float
    {
        return $this->length;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return sprintf("%f", $this->length);
    }
}
