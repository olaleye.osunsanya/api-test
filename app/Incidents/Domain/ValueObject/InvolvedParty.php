<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\ValueObject;

use Fusion\Common\Domain\Model\ValueObject;

final class InvolvedParty extends ValueObject
{

    /** @var $id */
    private $id;

    /** @var $name */
    private $name;

    /** @var $involvement */
    private $involvement;

    /**
     * @param string $id
     * @param string $name
     * @param string $involvement
     */
    public function __construct(string $id, string $name, $involvement)
    {
        $this->id = $id;
        $this->name = $name;
        $this->involvement = $involvement;
    }

    /** @return string */
    public function getId(): string
    {
        return $this->id;
    }

    /** @return string */
    public function getName()
    {
        return $this->name;
    }

    /** @return string */
    public function getInvolvement()
    {
        return $this->involvement;
    }

    public function __toString(): string
    {
        return sprintf("id : %s, name : %s, involvement : %s", $this->id, $this->name, $this->involvement);
    }
}
