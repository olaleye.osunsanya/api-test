<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\ValueObject;

use Fusion\Common\Domain\Model\ValueObject;

final class IncidentCategory extends ValueObject
{

    /**@var string */
    protected $id;

    /**@var string */
    protected $name;

    /**@var string */
    protected $colour;

    /**
     * @param string $id
     * @param string $name
     * @param string $colour
     */
    public function __construct(string $id, string $name, string $colour)
    {
        $this->id = $id;
        $this->name = $name;
        $this->colour = $colour;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getColour(): string
    {
        return $this->colour;
    }

    public function __toString(): string
    {
        return sprintf("id : %s, name : %s, colour : %s", $this->id, $this->name, $this->colour);
    }
}
