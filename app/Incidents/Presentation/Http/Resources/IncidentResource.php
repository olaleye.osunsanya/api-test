<?php

declare(strict_types=1);

namespace Fusion\Incidents\Presentation\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IncidentResource extends JsonResource
{
    /**
     * Transform the resource into an array
     * 
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId()->toString(),
            'description' => $this->getDescription(),
            'reportedAt' => $this->getReportedAt(),
            'category' => (object) [
                'id' => $this->getCategory()->getId(),
                'name' => $this->getCategory()->getName(),
                'colour' => $this->getCategory()->getColour(),
            ],
            'position' => (object) [
                'longitude' => $this->getPosition()->latitude(),
                'latitude' => $this->getPosition()->longitude(),
            ],
            'involvedParty' => (object)[
                'id' => $this->getInvolvedParty()->getId(),
                'name' => $this->getInvolvedParty()->getName(),
                'involvement' => $this->getInvolvedParty()->getInvolvement()
            ]
        ];
    }
}
