<?php

declare(strict_types=1);

namespace Fusion\Incidents\Presentation\Http\Controller;

use Fusion\Common\Application\Query\DispatchesQueries;
use Fusion\Incidents\Application\Command\CreateIncidentCommand;
use Fusion\Incidents\Application\Query\GetIncidentQuery;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\QueryBus;
use Symfony\Component\HttpFoundation\Response;
use Fusion\Incidents\Presentation\Http\Resources\IncidentResource;

class IncidentsController
{
    use DispatchesQueries;

    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus, QueryBus $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }

    public function store(Request $request): Response
    {
        $commmand = new CreateIncidentCommand(json_decode($request->getContent()));

        $this->commandBus->dispatch($commmand);

        return new JsonResponse(
            [
                'incidentId' => $commmand->getIncidentId()->toString(),
            ],
            Response::HTTP_CREATED
        );
    }

    public function show(string $id): Response
    {
        $query = GetIncidentQuery::byId($id);

        $incident = $this->dispatchQuery($query);

        return new JsonResponse(new IncidentResource($incident), Response::HTTP_OK);
    }
}
